export const environment = {
  production: true,
  baseUrlApp: 'http://localhost:57741',
  paramsGetAll: { page: 1, row: 100 }
};
