import { userConstant } from './../../constants/user.constant';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../../data/entities/user';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { LoginHandlerService } from '../commons/login-handler.service';
import { environment } from 'src/environments/environment';
import { GenericsAppsServiceAci } from '../generics/apps';
import { IPagiResult } from 'src/app/core/Pagination/IPagiResult';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userSubject: BehaviorSubject<any> = new BehaviorSubject({});
  baseUrl: string;

  constructor(private genericsApps: GenericsAppsServiceAci) {
    this.baseUrl = environment.baseUrlApp;
  }

  findAll(): Observable<any> {
    const data = 'page=' + 1 + '&row=' + environment.paramsGetAll.row;

    return this.genericsApps.findAll(userConstant.urlApi + '?' + data, []);
  }

  find(id: number) {
    return this.genericsApps.findById(userConstant.urlApi, id);
  }

  // find(id: any): Observable<User> {
  //     return this.http.get<any>(this.baseUrlApp + '/api/Utilisateurs/' + id, { headers: this.headers });
  // }

  setUser(user: User): void {
    this.userSubject.next(user);
  }

  getUser(): Observable<User> {
    return this.userSubject.asObservable();
  }
}
