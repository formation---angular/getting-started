import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public userConnete: BehaviorSubject<any> = new BehaviorSubject({});
  // public filariane: BehaviorSubject<string> = new BehaviorSubject(null);
  constructor() {}
}
