import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserToken } from 'src/app/data/entities/user-token';

@Injectable({
  providedIn: 'root'
})
export class LoginHandlerService {

  constructor() { }

  private userToken = new BehaviorSubject<UserToken>(null);

  /** Subscribe to authentication service   */
  public getUserToken(): Observable<UserToken> {
    return this.userToken.asObservable();
  }

  /** Publish user login info change */
  public setUserToken(userToken: UserToken): void {
    this.userToken.next(userToken);
  }
}
