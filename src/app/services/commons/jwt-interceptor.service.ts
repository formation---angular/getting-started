import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

// tslint:disable-next-line: max-line-length
import { HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpUserEvent, HttpProgressEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { catchError, filter, finalize, switchMap, take } from 'rxjs/operators';
import { UserToken } from 'src/app/data/entities/user-token';
import { AuthAppsServiceAci } from '../generics/apps/authentication/auth-app.service.aci';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService {
  isRefreshingToken = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private authServiceAppAci: AuthAppsServiceAci, private authService: AuthService, private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse |
  HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {
    const token: string = this.authService.getAuthToken();
    // add authorization header with jwt token if available
    return next.handle(this.addTokenToRequest(request, token))
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch ((err as HttpErrorResponse).status) {
              case 401:
                if (this.authService.isTokenExpired(token)) {
                  this.router.navigate(['/login']);
                  return this.handle401Error(request, next);
                }
                break;
              case 400:
                if (request.url.indexOf('/oauth/token') !== -1) {
                  this.authServiceAppAci.logout();
                  this.router.navigate(['/login']);
                }
                break;
            }
          }
          return throwError(err);
        }
      )
    );
  }

private addTokenToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {
  if (request.url.indexOf('/oauth/token') === -1) {
    return request.clone({
      headers: request.headers
        .set('Authorization', 'Bearer ' + token)
      // .set('Allow-origin', 'http://localhost:4200')
    });
  }
  return request.clone();
}

private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
  if (!this.isRefreshingToken) {
    this.isRefreshingToken = true;

    this.tokenSubject.next(null);

    return this.authServiceAppAci.refreshToken()
      .pipe(
        switchMap((user: UserToken) => {
          if (user && user.token) {
            this.tokenSubject.next(user.token);
            return next.handle(this.addTokenToRequest(request, user.token));
          }
          return this.authServiceAppAci.logout() as any;
        }),
        catchError(err => {
          return this.authServiceAppAci.logout() as any;
        }),
        finalize(() => {
          this.isRefreshingToken = false;
        })
      );
  } else {
    this.isRefreshingToken = false;

    return this.tokenSubject
      .pipe(filter(token => token != null),
        take(1),
        switchMap(token => {
          return next.handle(this.addTokenToRequest(request, token));
        }));
  }
}
}
