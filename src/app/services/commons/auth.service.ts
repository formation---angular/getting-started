import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

import { UserToken } from 'src/app/data/entities/user-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  helper = new JwtHelperService();
  constructor() { }

  getCurrentUser(): UserToken {
    return JSON.parse(localStorage.getItem('currentUser')) as UserToken;
  }

  getAuthToken(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser')) as UserToken;
    if (currentUser != null) {
      return currentUser.token;
    }

    return '';
  }

  getPayload(token: string): any {
    if (token) {
      return this.helper.decodeToken(token);
    }
  }

  isTokenExpired(token: string): boolean {
    return this.helper.isTokenExpired(token);
  }

  getAutorisation(view, action) {
    const auth = this.getPayload(this.getCurrentUser().token);
    return auth[view] && action;
  }

  getAutorisationObj(viewObj, action) {
    const auth = this.getPayload(this.getCurrentUser() && this.getCurrentUser().token);
    const result = viewObj && viewObj.map(element => auth && auth[element] && action);
    return result.includes(action);
  }
}
