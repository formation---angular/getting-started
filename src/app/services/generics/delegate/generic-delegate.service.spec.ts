import { TestBed } from '@angular/core/testing';

import { GenericDelegateService } from './generic-delegate.service';

describe('GenericDelegateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenericDelegateService = TestBed.get(GenericDelegateService);
    expect(service).toBeTruthy();
  });
});
