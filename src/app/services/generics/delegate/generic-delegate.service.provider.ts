import { GenericDelegateService } from './generic-delegate.service';
import { GenericDelegateServiceAci } from './generic-delegate.service.aci';

export let GenericDelegateServiceProvider = {
    provide: GenericDelegateServiceAci,
    useClass: GenericDelegateService
};
