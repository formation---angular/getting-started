import { Injectable } from '@angular/core';
import { GenericDelegateServiceAci } from './generic-delegate.service.aci';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GenericDelegateService implements GenericDelegateServiceAci {
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  baseUrlApp: string;

  constructor(private http: HttpClient) {
    this.baseUrlApp = environment.baseUrlApp;
  }

  public findAll(url: string, data: any) {
    const urlWithParams = this.createGetParams(url, data, '');
    return this.http.get<any>(this.baseUrlApp + urlWithParams, { headers: this.headers });
  }

  public findById(url: string, id: number) {
    return this.http.get<any>(this.baseUrlApp + url + '/' + id, { headers: this.headers });
  }

  public create(url: string, data: any) {
    return this.http.post<any>(this.baseUrlApp + url, data, { headers: this.headers });
  }

  public delete(url: string, id: number) {
    return this.http.delete<any>(this.baseUrlApp + url + '/' + id, { headers: this.headers });
  }

  public update(url: string, id: number, data: any) {
    return this.http.put<any>(this.baseUrlApp + url + '/' + id, data, { headers: this.headers });
  }

  private createPostParams(data: any, defaultField: string = 'Id'): HttpParams {
    const params = new HttpParams();
    params.set('page', data.page == null ? '1' : data.page.toString())
      .set('row', data.row == null ? '10' : data.row.toString())
      .set('sortField', data.sortField == null ? defaultField : data.sortField)
      .set('orderAsc', data.orderAsc == null ? true : data.orderAsc);

    if (data) {
      Object.keys(data).forEach(key => {
        params.set(key, data[key] == null ? '' : data[key].toString());
      });
    }

    return params;
  }

  private createGetParams(url: string, data: any, defaultField: string = 'Id'): string {
    if (data) {
      const fields = Object.keys(data);
      if (!fields) {
        return;
      }

      const pageIndex = fields.findIndex(f => f === 'page');
      if (pageIndex !== -1) {
        url += data.page ? '?page=' + data.page.toString() : '?page=1';
        fields.splice(pageIndex, 1);
      }

      fields.forEach(key => {
        url += data[key] == null ? '' : '&' + key + '=' + data[key].toString();
      });
    }

    return url;
  }

  public findByParams(url: string, data: any) {
    const keyData = Object.keys(data);
    let httpparams = new HttpParams();
    for (const key of keyData) {
      httpparams = httpparams.set(key, data[key]);
    }
    const httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.get<any>(this.baseUrlApp + url, { headers: httpHeaders, params: httpparams });
  }
}
