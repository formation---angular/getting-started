import { TestBed } from '@angular/core/testing';

import { AuthenticationDelegateService } from './authentication.delegate.service';

describe('AuthenticationDelegateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthenticationDelegateService = TestBed.get(AuthenticationDelegateService);
    expect(service).toBeTruthy();
  });
});
