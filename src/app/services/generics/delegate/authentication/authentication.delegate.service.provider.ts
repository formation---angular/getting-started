import { AuthenticationDelegateService } from './authentication.delegate.service';
import { AuthenticationDelegateServiceAci } from './authentication.delegate.service.aci';

export let AuthenticationDelegateServiceProvider = {
    provide: AuthenticationDelegateServiceAci,
    useClass: AuthenticationDelegateService
};
