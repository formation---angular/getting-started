import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthenticationDelegateServiceAci } from './authentication.delegate.service.aci';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationDelegateService implements AuthenticationDelegateServiceAci {
  private headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
  baseUrlApp: string;

  constructor(private http: HttpClient) {
    this.baseUrlApp = environment.baseUrlApp;
  }

  login(username: string, password: string) {
    const loginUrl = this.baseUrlApp + '/oauth/token';
    const loginData = 'username=' + username + '&password=' + password + '&grant_type=password';

    return this.http.post<any>(loginUrl, loginData, {headers: this.headers});
  }

  refreshToken(refreshToken: string) {
    const refreshUrl = this.baseUrlApp + '/oauth/token';
    const refreshData = 'grant_type=refresh_token&refresh_token=' + refreshToken;
    return this.http.post<any>(refreshUrl, refreshData, {headers: this.headers});
  }
}
