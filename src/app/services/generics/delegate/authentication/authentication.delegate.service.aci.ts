import { Injectable } from '@angular/core';

@Injectable()
export abstract class AuthenticationDelegateServiceAci {
    public abstract login(username: string, password: string);
    public abstract refreshToken(refreshToken: string);
}
