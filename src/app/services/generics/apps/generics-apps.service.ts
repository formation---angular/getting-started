import { Injectable } from '@angular/core';
import { GenericsAppsServiceAci } from './generics-apps.service.aci';
import { GenericBusinessServiceAci } from '../business';

@Injectable({
  providedIn: 'root'
})
export class GenericsAppsService implements GenericsAppsServiceAci {
  constructor(private genericBusiness: GenericBusinessServiceAci) { }

  public findAll(url: string, data: any) {
    return this.genericBusiness.findAll(url, data);
  }

  public findById(url: string, id: number) {
    return this.genericBusiness.findById(url, id);
  }

  public create(url: string, data: any) {
    return this.genericBusiness.create(url, data);
  }

  public delete(url: string, id: number) {
    return this.genericBusiness.delete(url, id);
  }

  public update(url: string, id: number, data: any) {
    return this.genericBusiness.update(url, id, data);
  }

  public findByParams(url: string, data: any) {
    return this.genericBusiness.findByParams(url, data);
  }
}
