import { GenericsAppsService } from './generics-apps.service';
import { GenericsAppsServiceAci } from './generics-apps.service.aci';

export let GenericAppsServiceProvider = {
    provide: GenericsAppsServiceAci,
    useClass: GenericsAppsService
};
