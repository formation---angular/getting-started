import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export abstract class GenericsAppsServiceAci {
    abstract findAll(url: string, data: any);
    abstract findById(url: string, id: number);
    abstract findByParams(url: string, data: any);
    abstract create(url: string, data: any);
    abstract delete(url: string, id: number);
    abstract update(url: string, id: number, data: any);
}
