import { TestBed } from '@angular/core/testing';

import { AuthAppsService } from './auth-apps.service';

describe('AuthAppsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthAppsService = TestBed.get(AuthAppsService);
    expect(service).toBeTruthy();
  });
});
