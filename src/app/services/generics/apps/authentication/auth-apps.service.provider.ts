import { AuthAppsService } from './auth-apps.service';
import { AuthAppsServiceAci } from './auth-app.service.aci';

export let AuthAppsServiceProvider = {
    provide: AuthAppsServiceAci,
    useClass: AuthAppsService
};
