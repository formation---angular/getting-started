import { Injectable } from '@angular/core';
import { AuthenticationBusinessServiceAci } from '../../business/authentication/authentication.business.service.aci';

@Injectable({
  providedIn: 'root'
})
export class AuthAppsService {
  constructor(
    private authBusiness: AuthenticationBusinessServiceAci,
  ) {}

  login(username: string, password: string) {
    return this.authBusiness.login(username, password);
  }

  refreshToken() {
    return this.authBusiness.refreshToken();
  }

  logout() {
    return this.authBusiness.logout();
  }
}
