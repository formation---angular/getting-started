import { TestBed } from '@angular/core/testing';

import { GenericBusinessService } from './generic-business.service';

describe('GenericBusinessService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenericBusinessService = TestBed.get(GenericBusinessService);
    expect(service).toBeTruthy();
  });
});
