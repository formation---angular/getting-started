import { GenericBusinessServiceAci } from './generic-business.service.aci';
import { GenericBusinessService } from './generic-business.service';

export let GenericBusinessServiceProvider = {
    provide: GenericBusinessServiceAci,
    useClass: GenericBusinessService
};
