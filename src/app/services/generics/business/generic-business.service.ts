import { Injectable } from '@angular/core';
import { GenericBusinessServiceAci } from './generic-business.service.aci';
import { GenericDelegateService } from '../delegate';
import { tap, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ErrorManager } from 'src/app/core/error/errorManager';

@Injectable({
  providedIn: 'root'
})
export class GenericBusinessService implements GenericBusinessServiceAci {
  protected delegateService: GenericDelegateService;

  constructor(private http: HttpClient) {
    this.delegateService = new GenericDelegateService(this.http);
  }

  public findAll(url: string, data: any) {
    return this.delegateService.findAll(url, data)
      .pipe(
        tap(_ => { }),
        catchError(ErrorManager.handleError));
  }

  public findById(url: string, id: number) {
    return this.delegateService.findById(url, id)
      .pipe(
        tap(_ => { }),
        catchError(ErrorManager.handleError));
  }

  public create(url: string, data: any) {
    return this.delegateService.create(url, data)
      .pipe(
        tap(_ => { }),
        catchError(ErrorManager.handleError));
  }

  public delete(url: string, id: number) {
    return this.delegateService.delete(url, id)
      .pipe(
        tap(_ => { }),
        catchError(ErrorManager.handleError));
  }

  public update(url: string, id: number, data: any) {
    return this.delegateService.update(url, id, data)
      .pipe(
        tap(_ => { }),
        catchError(ErrorManager.handleError));
  }

  public findByParams(url: string, data: any) {
    return this.delegateService.findByParams(url, data)
      .pipe(
        tap(_ => { }),
        catchError(ErrorManager.handleError));
  }
}
