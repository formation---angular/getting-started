import { Injectable } from '@angular/core';

import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticationBusinessServiceAci } from './authentication.business.service.aci';
import { AuthenticationDelegateServiceAci } from '../../delegate/authentication/authentication.delegate.service.aci';
import { LoginHandlerService } from '../../../commons/login-handler.service';
import { AuthService } from '../../../commons/auth.service';
import { UserToken } from 'src/app/data/entities/user-token';
import { Observable } from 'rxjs/internal/Observable';
import { catchError, map } from 'rxjs/operators';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationBusinessService implements AuthenticationBusinessServiceAci {
  jwtHelper: JwtHelperService = new JwtHelperService();
  constructor(
    private authenticationDelegateServiceACI: AuthenticationDelegateServiceAci,
    private handler: LoginHandlerService,
    private oautService: AuthService
  ) { }

  public login(username: string, password: string): Observable<UserToken> {
    return this.authenticationDelegateServiceACI.login(username, password)
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.access_token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          const currentUser: UserToken = this.toUserToken(user);
          localStorage.setItem('currentUser', JSON.stringify(currentUser));
          this.handler.setUserToken(currentUser);
        }

        return user;
      }, catchError(this.handleError));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.handler.setUserToken(null);
  }

  refreshToken(): Observable<UserToken> {
    let currentUser = this.oautService.getCurrentUser();
    const refreshToken = currentUser.refreshToken;
    return this.authenticationDelegateServiceACI.refreshToken(refreshToken).
      pipe(
        catchError(this.handleError),
        map((user: any) => {
          if (user && user.access_token) {
            currentUser = this.toUserToken(user);
            localStorage.setItem('currentUser', JSON.stringify(currentUser));
            this.handler.setUserToken(currentUser);
          }
          return currentUser;
        })
      );
  }

  private handleError(error) {
    // tslint:disable-next-line: deprecation
    return Observable.throw(
      error && {
        status: error.status,
        message: (error.error && error.error.message) || error.statusText
      }
    );
  }

  private toUserToken(user: any): UserToken {
    const currentUser: UserToken = new UserToken();
    currentUser.id = user.id;
    currentUser.token = user.access_token;
    currentUser.refreshToken = user.refresh_token;
    currentUser.username = user.username;
    currentUser.expiresIn = user.expires_in;
    currentUser.tokenType = user.token_type;
    return currentUser;
  }
}
