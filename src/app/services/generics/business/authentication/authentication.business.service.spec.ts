import { TestBed } from '@angular/core/testing';
import { AuthenticationBusinessService } from './authentication.business.service';


describe('AuthenticationBusinessService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthenticationBusinessService = TestBed.get(AuthenticationBusinessService);
    expect(service).toBeTruthy();
  });
});
