import { AuthenticationBusinessServiceAci } from './authentication.business.service.aci';
import { AuthenticationBusinessService } from './authentication.business.service';

export let AuthenticationBusinessServiceProvider = {
    provide: AuthenticationBusinessServiceAci,
    useClass: AuthenticationBusinessService
};
