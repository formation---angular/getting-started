import { Injectable } from '@angular/core';

@Injectable()
export abstract class AuthenticationBusinessServiceAci {
  public abstract login(username: string, password: string);
  public abstract refreshToken();
  public abstract logout();
}
