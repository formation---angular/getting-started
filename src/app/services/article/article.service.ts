import { articleConstant } from './../../constants/article.constant';
import { environment } from './../../../environments/environment';
import { ARTICLES } from './../../data/mocks/articles.mock';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Article } from './../../data/entities/article';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IPagiResult } from 'src/app/core/Pagination/IPagiResult';
import { GenericsAppsServiceAci } from '../generics/apps';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private articleSubject: BehaviorSubject<any> = new BehaviorSubject({});
  private baseUrl = environment.baseUrlApp + articleConstant.urlApi;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private httpClient: HttpClient, private genericsApps: GenericsAppsServiceAci) { }

  findAll(): Observable<IPagiResult<Article>> {
    const data = 'page=' + 1 + '&row=' + environment.paramsGetAll.row;

    return this.httpClient.get<IPagiResult<Article>>(this.baseUrl + '?' + data, { headers: this.headers });
  }

  setArticle(article: Article): void {
    this.articleSubject.next(article);
  }

  getArticle(): Observable<Article> {
    return this.articleSubject.asObservable();
  }

  add(article: Article) {
    return this.genericsApps.create(articleConstant.urlApi, article);
    // this.articles.push(article);
    // this.emits();
  }

  update(id: number, article: Article) {
    return this.genericsApps.update(articleConstant.urlApi, id, article);
  }

  deleteById(id: number) {
    return this.genericsApps.delete(articleConstant.urlApi, id);
  }

  find(id: number) {
    return this.genericsApps.findById(articleConstant.urlApi, id);
  }
}
