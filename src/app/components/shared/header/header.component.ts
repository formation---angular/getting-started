import { AuthAppsServiceAci } from 'src/app/services/generics/apps/authentication/auth-app.service.aci';
import { SharedService } from 'src/app/services/commons/shared.service';
import { User } from './../../../data/entities/user';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: User;

  constructor(
    private sharedService: SharedService,
    private authAppsAci: AuthAppsServiceAci,
    private router: Router
    ) { }

  ngOnInit() {
    this.getNameUser();
   }

  getNameUser() {
    this.sharedService.userConnete.subscribe(res => {
      this.user = JSON.parse(localStorage.getItem('User'));
      console.log('user connected : ', this.user);

      // this.nom = res.nom;
      // this.prenom = res.prenom;
      // if (!this.nom || !this.prenom) {
      //   let UserName = JSON.parse(localStorage.getItem('UserName'));
      //   if (UserName) {
      //     this.nom = UserName.nom;
      //     this.prenom = UserName.prenom;
      //   }
      // }
    });
  }

  logOut() {
    this.authAppsAci.logout();
    localStorage.removeItem('User');
    this.sharedService.userConnete.next(null);
    this.router.navigate(['/login']);
  }
}
