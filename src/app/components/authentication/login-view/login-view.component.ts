
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/commons/auth.service';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/data/entities/user';
import { AuthAppsServiceAci } from 'src/app/services/generics/apps/authentication/auth-app.service.aci';
import { SharedService } from 'src/app/services/commons/shared.service';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css'],
})
export class LoginViewComponent implements OnInit {
  message1: string;
  message2: string;
  loginForm: FormGroup;
  submitted = false;
  loading = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authServiceApps: AuthAppsServiceAci,
    private authService: AuthService,
    private userService: UserService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.message1 = '';
    this.message2 = '';
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.loading = true;
    this.authServiceApps.login(this.f.username.value, this.f.password.value)
      .subscribe(() => {
          this.router.navigate(['/articles']);
          // Recupérer les infos de l'user connecté
          const userCurrent = this.authService.getCurrentUser();
          this.getInfosUserConnected(userCurrent.id);
        }, error => {
          // this.alertService.error(error);
          this.message1 = 'Nom d\'utilisateur non valide';
          this.message2 = 'Mot de passe non valide';
          this.loading = false;
        });
  }

  getInfosUserConnected(id) {
    this.userService.find(id).subscribe((res: User) => {
        console.log('user: ', res);
        this.sharedService.userConnete.next({ ...User });
        localStorage.setItem('User', JSON.stringify(res));
    });
  }

}
