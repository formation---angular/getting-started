import { IPagiResult } from '../../../core/Pagination/IPagiResult';
import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Article } from 'src/app/data/entities/article';
import { ArticleService } from 'src/app/services/article/article.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit, OnDestroy {
  articles: Article[];
  articleSelected: Article;
  // @Output() onarticleSelected: EventEmitter<Article> = new EventEmitter<Article>();

  constructor(private articleService: ArticleService, private router: Router) { }

  ngOnInit() {
    this.initData();
  }

  ngOnDestroy(): void {

  }

  initData() {
    this.articleService.findAll().subscribe((res: IPagiResult<Article>) => this.articles = res.Items);
  }

  articleClicked(article: Article): void {
    // this.articleService.setArticle(article);
    // this.onarticleSelected.emit(article);
    // this.articleSelected = article;
    // console.log('article : ', article);
    // this.router.navigate(['/articles', article.Id]);
  }

  onEdit(article: Article) {
    this.articleService.setArticle(article);
    // const found = this.articles.find(a => a.Id === article.Id);
    // console.log('onEdit : ', found);
    this.router.navigate(['/articles/update', article.Id]);
  }

  onDelete(id: number) {
    this.articleService.deleteById(id).subscribe(res => {
      const index = this.articles.findIndex(a => a.Id === id);
      if (res && index) {
        alert('Suppression réussie');
        this.articles.splice(index, 1);
      }
    });
  }
}
