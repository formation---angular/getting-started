import { ConfirmDialogService } from './../../../core/dialogs/confirmation/service/confirm-dialog.service';
import { Utils } from '../../../core/utilities/utils';
import { ArticleService } from '../../../services/article/article.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Article } from 'src/app/data/entities/article';
import { numberValidator } from 'src/app/validators/number.validator';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {
  @Input() article: any;
  articleForm: FormGroup;
  btnLabel = 'Créer';
  private articleId?: number;

  constructor(
    private formBuilder: FormBuilder,
    private articleService: ArticleService,
    private confirmDialogService: ConfirmDialogService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    try {
      this.initForm();
      // si c'est un ajout alors exit
      this.articleId = this.route.snapshot.params && this.route.snapshot.params.id;
      if (!this.articleId) { return; }

      this.updateDataInit();
    } catch (error) {
      console.log('ERROR : ', error);
    }
  }

  initForm() {
    this.articleForm = this.formBuilder.group({
      Id: '',
      Nom: ['', Validators.required],
      PrixUnitaire: ['', Validators.compose([Validators.required, numberValidator])],
      Reference: '',
      RedactionId: ''
    });
  }

  onSubmitForm() {
    const formValue = this.articleForm.value;
    if (!formValue) { return; }

    const article = new Article(
      this.articleId,
      formValue.Nom,
      formValue.PrixUnitaire,
      formValue.RedactionId,
      formValue.Reference
    );

    console.log('form values : ', article);
    this.onCreateOrUpdate(article);
  }

  onCreateOrUpdate(article: Article) {
    if (this.articleId) { // update
      this.articleService.update(this.articleId, article).subscribe((res: Article) => {
        if (!res || !res.Id) { return; }
        alert('Modification réussie');
        this.router.navigate(['/articles']);
      });
    } else { // create
      this.articleService.add(article).subscribe((res: Article) => {
        if (!res || !res.Id) { return; }
        // TODO:: doit être un popup et utiliser push au lieu de findAll()
        this.router.navigate(['/articles']);
      });
    }
  }

  updateFormInit(article: Article) {
    this.articleForm.patchValue(article);
    this.btnLabel = 'Modifier';
  }

  updateDataInit() {
    // TODO: utiliser async/await pour éviter le cbHell
    this.articleService.getArticle().subscribe(art => {
      this.article = art;
      // si art n'est pas un obj vide
      if (!Utils.objectIsEmpty(art)) {
        this.updateFormInit(this.article);
        return;
      }
      // si art est vide alors récupérer l'article par id
      this.articleService.find(this.articleId).subscribe(a => {
        this.article = a;
        this.updateFormInit(this.article);
      });
      // initialisation du formulaire
      // this.initForm(this.article.Id);
    });
  }

  get Nom() {
    return this.articleForm.get('Nom');
  }

  showDialog() {
    console.log('showdialog');
    this.confirmDialogService.confirmThis('Êtes-vous sûr de vouloir supprimer ?', () => {
      console.log('Yes clicked');
    }, () => {
      console.log('No clicked');
    });
  }
}
