import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ArticleComponent } from './article/article.component';
import { ArticleDetailsComponent } from './article-details/article-details.component';
import { CreateArticleComponent } from './create-article/create-article.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ ArticleComponent, ArticleDetailsComponent, CreateArticleComponent ],
  imports: [ CommonModule, RouterModule, ReactiveFormsModule, FormsModule ],
  exports: [ ArticleComponent, ArticleDetailsComponent ],
})
export class ArticlesModule { }
