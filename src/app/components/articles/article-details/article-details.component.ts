import { Component, OnInit, Input } from '@angular/core';
import { ArticleService } from 'src/app/services/article/article.service';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'src/app/data/entities/article';
import { Utils } from 'src/app/core/utilities/utils';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.css']
})
export class ArticleDetailsComponent implements OnInit {
  @Input() article: any;

  constructor(private articleService: ArticleService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.articleService.getArticle().subscribe((art: Article) => {
      if (!Utils.objectIsEmpty(art)) {
        this.article = art;
      } else {
        const id = this.route.snapshot.params && this.route.snapshot.params.id;
        this.articleService.find(id).subscribe((art2: Article) => this.article = art2);
      }
    });

  }
}
