import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonComponent } from './user-list/person.component';
import { PersonDetailsComponent } from './user-details/person-details.component';
import { UserService } from 'src/app/services/user/user.service';
import { CreateUserComponent } from './create-user/create-user.component';

@NgModule({
  declarations: [ PersonComponent, PersonDetailsComponent, CreateUserComponent ],
  imports: [ CommonModule, RouterModule ],
  exports: [ PersonComponent, PersonDetailsComponent, CreateUserComponent ],
  providers: [ UserService ]
})
export class UserModule { }
