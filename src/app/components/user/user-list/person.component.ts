import { Router } from '@angular/router';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/data/entities/user';
import { IPagiResult } from 'src/app/core/Pagination/IPagiResult';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  users: User[];
  selectedUser: User;
  classActive: string;

  // @Output() onselectUser = new EventEmitter<User>();

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.findAll().subscribe((res) => {
      this.users = res.Items;
    });
  }

  userClicked(user: User): void {
    // this.userService.setUser(user);
    // // this.onselectUser.emit(user);
    // // this.selectedUser = user;
    // this.classActive = 'active';
    // this.router.navigate(['/users', user.Id]);
  }

  onEdit(user: User) {
    this.userService.setUser(user);
    console.log('user update');
    // const found = this.articles.find(a => a.Id === article.Id);
    // console.log('onEdit : ', found);
    // this.router.navigate(['/users/update', user.Id]);
  }

  onDelete(id: number) {
    // this.userService.deleteById(id).subscribe(res => {
    //   const index = this.articles.findIndex(a => a.Id === id);
    //   if (res && index) {
    //     alert('Suppression réussie');
    //     this.articles.splice(index, 1);
    //   }
    // });
  }
}
