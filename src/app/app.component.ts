import { Article } from 'src/app/data/entities/article';
import { User } from 'src/app/data/entities/user';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'One way binding';
  isDisabled = false;
  userSelected: User;
  articleSelected: Article;

  eventClick(): void {
    console.log('btn clické');
  }

  onUserSelected(user: User): void {
    this.userSelected = user;
  }

  onArticleSelected(article: Article): void {
    this.articleSelected = article;
  }
}
