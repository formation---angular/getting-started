export class Utils {
  public static objectIsEmpty(obj: any) {
    return Object.entries(obj).length === 0 && obj.constructor === Object;
  }
}
