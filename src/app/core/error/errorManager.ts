import { Observable } from 'rxjs';

export class ErrorManager {
  public static handleError(error: any) {
    // tslint:disable-next-line: deprecation
    return Observable.throw(
      error && error.error
    );
  }
}
