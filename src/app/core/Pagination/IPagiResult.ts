export interface IPagiResult<T> {
    Items: T[];
    Count: number;
    TotalRowCount: number;
    CurrentPage: number;
    RowPerPage: number;
}
