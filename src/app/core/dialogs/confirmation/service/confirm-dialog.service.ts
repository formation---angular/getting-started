import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDialogService {
  private subject: BehaviorSubject<any> = new BehaviorSubject({});

  constructor() { }

  confirmThis(message: string, siFn: () => void, noFn: () => void) {
    this.setConfirmation(message, siFn, noFn);
  }

  setConfirmation(message: string, siFn: () => void, noFn: () => void) {
    this.subject.next({
      type: 'confirm',
      text: message,
      siFn() {
          this.subject.next(); // this will close the modal
          siFn();
        },
      noFn() {
        this.subject.next();
        noFn();
      }
    });
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
