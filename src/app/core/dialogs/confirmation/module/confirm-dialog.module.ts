import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ConfirmDialogService } from '../service/confirm-dialog.service';
import { ConfirmDialogComponent } from '../component/confirm-dialog.component';


@NgModule({
  declarations: [ ConfirmDialogComponent ],
  imports: [ CommonModule ],
  exports: [ ConfirmDialogComponent ],
  providers: [ ConfirmDialogService ]
})
export class ConfirmDialogModule { }
