import { Component, OnInit } from '@angular/core';
import { ConfirmDialogService } from '../service/confirm-dialog.service';


@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {
  message: any;

  constructor(private confirmDialogService: ConfirmDialogService) { }

  ngOnInit() {
    // cette fonction attend un message du service d'alerte
    // , elle est déclenchée lorsque nous appelons cela depuis n'importe quel autre composant
    this.confirmDialogService.getMessage().subscribe(message => {
      this.message = message;
    });
  }
}
