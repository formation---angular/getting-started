import { GenericAppsServiceProvider } from './services/generics/apps/generics-apps.service.provider';
import { GenericBusinessServiceProvider } from './services/generics/business/generic-business.service.provider';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { UserModule } from './components/user/user.module';
import { ArticlesModule } from './components/articles/articles.module';
import { AppRoutesModule } from './routing/app-routes/app-routes.module';
import { JwtInterceptorService } from './services/commons/jwt-interceptor.service';
import { AuthenticationModule } from './components/authentication/authentication.module';

import { AuthAppsServiceProvider } from './services/generics/apps/authentication/auth-apps.service.provider';
// tslint:disable-next-line: max-line-length
import { AuthenticationDelegateServiceProvider } from './services/generics/delegate/authentication/authentication.delegate.service.provider';
import { GenericDelegateServiceProvider } from './services/generics/delegate/generic-delegate.service.provider';
// tslint:disable-next-line: max-line-length
import { AuthenticationBusinessServiceProvider } from './services/generics/business/authentication/authentication.business.service.provider';
import { ConfirmDialogModule } from './core/dialogs/confirmation/module/confirm-dialog.module';
import { HeaderComponent } from './components/shared/header/header.component';


@NgModule({
  declarations: [ AppComponent, HeaderComponent ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthenticationModule,
    UserModule,
    ArticlesModule,
    AppRoutesModule,
    ConfirmDialogModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true },
    AuthenticationDelegateServiceProvider,
    AuthenticationBusinessServiceProvider,
    AuthAppsServiceProvider,
    GenericDelegateServiceProvider,
    GenericBusinessServiceProvider,
    GenericAppsServiceProvider
   ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
