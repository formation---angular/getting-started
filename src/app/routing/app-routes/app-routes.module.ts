import { CreateArticleComponent } from '../../components/articles/create-article/create-article.component';
import { ArticleDetailsComponent } from '../../components/articles/article-details/article-details.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PersonComponent } from 'src/app/components/user/user-list/person.component';
import { ArticleComponent } from 'src/app/components/articles/article/article.component';
import { PersonDetailsComponent } from 'src/app/components/user/user-details/person-details.component';
import { LoginViewComponent } from 'src/app/components/authentication/login-view/login-view.component';
import { CreateUserComponent } from 'src/app/components/user/create-user/create-user.component';


const routes = [
  { path: 'login', component: LoginViewComponent },
  { path: 'users', component: PersonComponent, pathMatch: 'full' },
  { path: 'users/create', component: CreateUserComponent, pathMatch: 'full' },
  { path: 'users/:id', component: PersonDetailsComponent, pathMatch: 'full' },
  { path: 'articles', component: ArticleComponent, pathMatch: 'full' },
  { path: 'articles/create', component: CreateArticleComponent, pathMatch: 'full' },
  { path: 'articles/update/:id', component: CreateArticleComponent, pathMatch: 'full' },
  { path: 'articles/:id', component: ArticleDetailsComponent, pathMatch: 'full' }
];

@NgModule({
  declarations: [],
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutesModule { }
