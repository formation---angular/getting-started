export class UserToken {
    id: number;
    token: string;
    refreshToken: string;
    username: string;
    expiresIn: number;
    tokenType: string;
}
