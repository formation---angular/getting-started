export class Role {
  constructor(
    public Id: number,
    public Name: string,
    public Description?: string
  ) { }
}
