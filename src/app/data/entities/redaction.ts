import { Article } from './article';

export class Redaction {
  constructor(
    public Id: number,
    public EstPublie: boolean,
    public Statut: string,
    public Date: Date,
    public Articles: Article[]
  ) { }
}
