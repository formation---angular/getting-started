import { Redaction } from './redaction';
import { User } from './user';

export class Article {
  constructor(
    public Id: number,
    public Nom: string,
    public PrixUnitaire: number,
    public RedactionId?: number,
    public Reference?: string,
    // tslint:disable-next-line: no-shadowed-variable
    public Redaction?: Redaction
  ) { }
}
