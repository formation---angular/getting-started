import { Role } from './role';
export class User {
  constructor(
    public Id: number,
    public Nom: string,
    public Mail: string,
    public Active: boolean,
    public Prenom?: string,
    public RoleId?: number,
    // tslint:disable-next-line: no-shadowed-variable
    public Role?: Role
  ) { }
}
